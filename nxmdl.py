#!/usr/bin/env python3
#
# Nexus Mod Downloader – assists with downloading mod files from Nexus Mods
# Copyright © 2019 Frederik “Freso” S. Olesen <https://freso.dk/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Downloads files from Nexus Mods."""

import os

import pynxm
import requests
import xdgenvpy

from urllib.parse import urlparse, parse_qsl


def read_config():
    """Read configuration file and return parsed configuration."""
    import toml
    xdg = xdgenvpy.XDGPackage('nxmdl')
    config_file = os.path.join(xdg.XDG_CONFIG_HOME, 'nxmdl.toml')

    try:
        cfg = toml.load(config_file)
    except FileNotFoundError:
        print('Configuration file ‘{}’ doesn’t exist. '
              'Be sure to create this before using nxmdl.'.format(config_file))
        raise
    except PermissionError:
        print('You don’t have permission to read ‘{}’.'.format(config_file))
        raise

    if not cfg['nexusmods'].get('apikey'):
        print('Nexus Mods API key is not set.')
        # TODO: Add and raise appropriate exception/error?
        exit(1)

    return cfg


def convert_nxm_to_http(nxm_url, api_key):
    """Convert an nxm:// URL to a http(s):// one."""
    # Sample: nxm://Fallout4/mods/33484/files/140950?key=ye4498C2qEqcAeXsArPY9w&expires=1560894810&user_id=3801260

    parsed_nxm_url = urlparse(nxm_url)
    nxm_game = parsed_nxm_url.netloc.lower()
    nxm_mod = parsed_nxm_url.path.split('/')[2]
    nxm_file = parsed_nxm_url.path.split('/')[4]
    nxm_url_qs = dict(parse_qsl(parsed_nxm_url.query))
    nxm_key = nxm_url_qs.get('key', None)
    nxm_expiry = nxm_url_qs.get('expires', None)

    nxm = pynxm.Nexus(api_key)
    return nxm.mod_file_download_link(nxm_game, nxm_mod, nxm_file,
                                      nxm_key, nxm_expiry)[0]


def download_file(file_url, dest_path, meta=False):
    """Downloads a given file."""
    file_name = urlparse(file_url).path.split('/')[-1]
    dest_file = os.path.join(dest_path, file_name)
    try:
        file_size = os.path.getsize(dest_file)
    except FileNotFoundError:
        file_size = 0

    if meta:
        meta_file = dest_file = '.meta'

    with requests.get(file_url,
                      headers={'Range': 'bytes={}-'.format(file_size)},
                      stream=True, allow_redirects=True) as r:
        r.raise_for_status()
        with open(dest_file, 'ab') as fd:
            for chunk in r.iter_content(chunk_size=512*1024):
                fd.write(chunk)


def argument_parser(args):
    """Parse (command‐line) arguments."""
    from argparse import ArgumentParser
    parser = ArgumentParser()
    # TODO: Implement type function/class for nxm_url
    parser.add_argument('url', default=None,
                        help="The nxm:// URL to download.")
    return parser.parse_args(args)


def main(args):
    cfg = read_config()
    opts = argument_parser(args)
    dl_url = convert_nxm_to_http(opts.url, cfg['nexusmods']['apikey'])
    dest = os.path.expanduser(cfg['games']['location'])
    try:
        os.makedirs(dest, mode=0o755)
    except FileExistsError:
        pass
    download_file(dl_url['URI'], dest)


if __name__ == "__main__":
    from sys import argv
    main(argv[1:])
