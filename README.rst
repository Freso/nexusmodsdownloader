Nexus Mods Downloader
=====================

Utility to assist with downloading mods from `Nexus Mods`_.
Meant to be compatible with `Mod Organizer 2`_.

.. _Nexus Mods: https://www.nexusmods.com/
.. _Mod Organizer 2: https://www.nexusmods.com/skyrimspecialedition/mods/6194

Motivation
----------

`Nexus Mods`_ is the premiere site for getting game mods (modifications)
for a number of games, notably the Bethesda role playing games in the
Fallout_ and `The Elder Scrolls`_ series.

Most of these games can be made to run under Linux, but the current
modding utility ecosystem is very heavily Windows skewed. This utility
is just a small piece in the puzzle that will hopefully make modding of
these games in a Linux environment just a little bit nicer.

.. _Fallout: https://en.wikipedia.org/wiki/Fallout_(series)
.. _The Elder Scrolls: https://en.wikipedia.org/wiki/The_Elder_Scrolls

Usage
-----

Note: First configure Nexus Mods Downloader by creating or modifying the
configuration file. See the “Configuration” section below.

Nexus Mods Downloader should register itself as an URL handler for
``nxm://`` URLs and should thus pop up as an option when opening such
URLs, e.g. in your browser.

Alternatively it can be used directly from the command‐line::

    nxmdl nxm://…

Configuration
-------------

Before using Nexus Mods Downloader you should configure it.
The configuration file is read from
``$XDG_CONFIG_HOME/nxmdl/nxmdl.toml``,
or, if that doesn’t exist (e.g., if ``$XDG_CONFIG_HOME`` is unset), from
``$HOME/.config/nxmdl/nxmdl.toml``.

See this example configuration for possible options:

Example configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: toml

   [nexusmods]
   # Personal API Key from https://www.nexusmods.com/users/myaccount?tab=api
   apikey = ''

   [modorganizer]
   # Whether to additionally save Mod Organizer compatible file metadata
   metafile = true

   [games]
   # Where mods will be downloaded to if no game specific configuration
   # is found. `{game}` can be used here to make different games go to
   # different directories. E.g.,
   location = '~/Downloads/Mods/{game}'
   # will resolve to "~/Downloads/Mods/fallout4" for Fallout 4 mods.
   # Default location is "$XDG_DOWNLOAD_DIR" or
   # "~/Downloads" if undefined.

   [games.skyrimspecialedition]
   # Where specifically Skyrim SE mods will be downloaded to.
   location = '~/Downloads/SSE/Mods'

Installation
------------

If you’re on Linux, check if your package repository has this.
If not, it can be installed through ``pip``::

    python3 -m pip install nxmdl

Testing
-------

First ensure that you have the dependencies needed to run tests::

    python3 -m pip install nxmdl[test]

Now you can run the tests using::

    python3 -m pytest

Contributing
------------

The project is still in its very early stages. Feel free to submit PRs,
but please adhere to code style and respect that not all the project
goals have been fully fleshed out yet, so your patch may not be in line
with the author’s vision.

Code style
^^^^^^^^^^

The program will have a strict adherence to `PEP 8`_, with the
exception of the line length. It is nice to stay under the 72/79
characters per line, but not an absolute necessity. Additionally, all
classes and functions should have docstrings adhering to `PEP 257`_,
using reStructuredText_ markup when plaintext is not expressive enough.

.. _PEP 8: https://www.python.org/dev/peps/pep-0008/
.. _PEP 257: https://www.python.org/dev/peps/pep-0257/
.. _reStructuredText: http://docutils.sourceforge.net/rst.html

Commits
^^^^^^^

Commits going into the master branch should strive to be
`atomic commits`_ with `good commit messages`_.

.. _atomic commits: https://www.freshconsulting.com/atomic-commits/
.. _good commit messages: https://chris.beams.io/posts/git-commit/

Contact
-------

The project is hosted on GitLab, and any bugs, feature requests, etc.
should go there: https://gitlab.com/Freso/nexusmodsdownloader

You can also discuss this project on `Freenode IRC`_ in the `##Freso
channel`_, or alternatively on Discord_.

.. _Freenode IRC: https://freenode.net/
.. _##Freso channel: https://webchat.freenode.net/?channels=##Freso
.. Maybe change to ircs://chat.freenode.net/##Freso at some point.
.. _Discord: https://discord.gg/hU85Grw


Acknowledgments
---------------

This program was made with the support of `my patrons on Patreon`_.
Thank you so much for supporting my work with open data and free and
open source software!

.. TODO: List individual patrons here…

.. _my patrons on Patreon: https://www.patreon.com/Freso

License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
